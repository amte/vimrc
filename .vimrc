set nocompatible              " required
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

""""""""""""""""""""""""" add all your plugins here (note older versions of Vundle
" used Bundle instead of Plugin)
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'kien/ctrlp.vim'
Plugin 'vim-scripts/indentpython.vim'
Plugin 'Raimondi/delimitMate'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-surround'
Plugin 'dyng/ctrlsf.vim'
Plugin 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plugin 'junegunn/fzf.vim'
Plugin 'Yggdroot/indentLine'
Plugin 'tpope/vim-fugitive'
Plugin 'supercollider/scvim'
Plugin 'ervandew/supertab'
Plugin 'terryma/vim-multiple-cursors'
Plugin 'tpope/vim-markdown'
Plugin 'dkprice/vim-easygrep'
"""""""""""""""""""""""""" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required



" let g:rbpt_max = 16
" let g:rbpt_loadcmd_toggle = 0
" au VimEnter * RainbowParenthesesToggle
" au Syntax * RainbowParenthesesLoadRound
" au Syntax * RainbowParenthesesLoadSquare
" au Syntax * RainbowParenthesesLoadBraces
" au Syntax * RainbowParenthesesLoadChevrons


let g:airline_theme='dark'

" You can also specify different areas of the 
" screen where the splits should occur by adding 
" the following lines to the .vimrc file:
set splitbelow
set splitright
"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" configure expanding of tabs for various file types
au BufRead,BufNewFile *.py set expandtab
au BufRead,BufNewFile *.c set expandtab
au BufRead,BufNewFile *.h set expandtab
au BufRead,BufNewFile Makefile* set noexpandtab

" --------------------------------------------------------------------------------
" configure editor with tabs and nice stuff...
" --------------------------------------------------------------------------------
set expandtab           " enter spaces when tab is pressed
set textwidth=120       " break lines when line length increases
set tabstop=4           " use 4 spaces to represent tab
set softtabstop=4
set shiftwidth=4        " number of spaces to use for auto indent
set autoindent          " copy indent from current line when starting a new line

" make backspaces more powerfull
set backspace=indent,eol,start

set ruler                           " show line and column number
syntax on               " syntax highlighting
set showcmd             " show (partial) command in status line

au BufNewFile, BufRead *.html, *.css
    \ set tabstop=2
    \ set softtabstop=2
    \ set shiftwidth=2
au BufRead, BufNewFile *.py, *.pyw, *.c, *.h match BadWhitespace /\s\+$/
" supercollider start
au BufEnter,BufWinEnter,BufNewFile,BufRead *.sc,*.scd set filetype=supercollider
au Filetype supercollider packadd scvim
" supercollider end
set encoding=utf-8
set is hls
set number
set autochdir
set shortmess-=S
set laststatus=2
" set cursorline
let g:airline_section_c = '%<%F%m %#__accent_red#%{airline#util#wrap(airline#parts#readonly(),0)}%#__restore__#'
hi MatchParen cterm=none ctermbg=red ctermfg=none
"
" then just press Ctrl-J whenever you want to split a line.
:nnoremap <NL> i<CR><ESC>

let g:sclangTerm = "lxterminal -e $SHELL -ic"
let g:scFlash = 1


filetype off
set runtimepath+=/home/amirt/lilypond/usr/share/lilypond/current/vim/
filetype on
syntax on


" xnoremap il g_o0
" onoremap il :normal vil<CR>
" xnoremap al $o0
" onoremap al :normal val<CR>
xnoremap il g_o^
onoremap il :normal vil<CR>
xnoremap al $o^
onoremap al :normal val<CR>


" Commentary
autocmd FileType lilypond setlocal commentstring=%\ %s
autocmd FileType supercollider setlocal commentstring=//\ %s


" https://github.com/tpope/vim-markdown.git
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
let g:markdown_syntax_conceal = 0
let g:markdown_minlines = 100



" Set visual mode bg
highlight Visual ctermbg=darkblue ctermfg=NONE

